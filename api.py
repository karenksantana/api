import flask 
from flask import request,jsonify


app= flask.Flask(__name__)
app.config["DEBUG"] = True


employer= [
	{
	   "employer_name": "Academia Jedi",
	   "employer_code": "4FqeyPrcbGNX",
	   "member_count": 5,
	   "thumbnail": "https://qrpoint.com.br/wp-content/uploads/2018/12/cropped-MarcaVertical-03-1.png",
	   "register_date": "04/09/2019"
	}
]

checkPin= [ 
	{
	    "pin_code": "12345"
	}
]

code=[
	{
   "member_name": "Lucas Skywalker",
   "member_code": 1,
   "member_personal_data": {
       "father": "Anaquim Skywalker",
       "mother": "Pâmela Amidala",
       "hand": "false"
   },
   "thumbnailHd": "https://qrpoint.com.br/wp-content/uploads/2018/12/cropped-MarcaVertical-03-1.png",
   "birthday": "04/05/1999"
}

]
attestation = [
	[
    {
        "initial_date": "01/10/2019",
        "final_date": "03/10/2019",
        "time": 65
    },
    {
        "inicial_date": "05/09/2019",
        "end_date": "05/10/2019",
        "time": 480
    }
]

]
@app.route('/employer', methods=['GET'])
def emp(employer):
	for employer in employers:
		return jsonify(employer)

@app.route('/api/v1/resources/pin', methods=['GET', 'POST'])
def check():
	data = request.json()
	code.append(data)

		return jsonify (data),201

@app.route ('/api/v1/resources/attestation', methods= ['GET'])
def attestation():
	return jsonify (attestation)


@app.route('/api/v1/resources/employer', methods=['GET'])
def api_employer():
	return jsonify(employer)

def api_id():
	if 'id' in request.args:
		id = int(request,args['id']) 
	else:
		return "Error: No id field provided"

	results = []

	for employer in employers:
		if employer['id'] == id:
			results.append(employer)

	return jsonify(results)

app.run()

